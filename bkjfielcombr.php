<?php
/**
 * BKJ Fiel - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   BKJ Fiel
 * @author    Agência COLLA <contato@agenciacolla.com.br>
 * @copyright 2024 Agência COLLA
 * @license   Proprietary https://agenciacolla.com.br
 * @link      https://agenciacolla.com.br
 *
 * @wordpress-plugin
 * Plugin Name: BKJ Fiel - mu-plugin
 * Plugin URI:  https://agenciacolla.com.br
 * Description: Customizations for bkjfiel.com.br site
 * Version:     1.0.7
 * Author:      Agência COLLA
 * Author URI:  https://agenciacolla.com.br/
 * Text Domain: bkjfielcombr
 * License:     Proprietary
 * License URI: https://agenciacolla.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('bkjfielcombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**************************************************************************************************************
 * Functions
 **************************************************************************************************************/

/**
 * Show Books
 * Return a array of bkjfiel_books custom post type
 */
function Show_BKJFiel_books() 
{
    if (post_type_exists('bkjfiel_book')) {
        $args = array(
            'posts_per_page'   => '-1',
            'orderby'          => 'title',
            'order'            => 'ASC',
            'post_type'        => 'bkjfiel_book',
            'post_status'      => 'publish',
            'suppress_filters' => true,
            'fields'           => '',
        );
        $books_posts = get_posts($args);

        $books = array();
        foreach ($books_posts as $book) {
            $books[$book->ID] = $book->post_title;
        }

        if (!empty($books)) {
            return $books;
        } else {
            return array(
                'none' => __('No books found', 'bkjfielcombr'),
            );
        }

    } else {
        return array(
            'none' => __('No books type', 'bkjfielcombr'),
        );
    } 
}

/**
 * Show Chapters
 * Return a array of bkjfiel_chapter custom post type
 */
function Show_BKJFiel_chapters() 
{
    if (post_type_exists('bkjfiel_chapter')) {
        $args = array(
            'posts_per_page'   => '-1',
            'orderby'          => 'title',
            'order'            => 'ASC',
            'post_type'        => 'bkjfiel_chapter',
            'post_status'      => 'publish',
            'suppress_filters' => true,
            'fields'           => '',
        );
        $chapters_posts = get_posts($args);

        $chapters = array();
        foreach ($chapters_posts as $chapter) {
            $chapters[$chapter->ID] = $chapter->post_title;
        }

        if (!empty($chapters)) {
            return $chapters;
        } else {
            return array(
                'none' => __('No chapters found', 'bkjfielcombr'),
            );
        }

    } else {                                    
        return array(
            'none' => __('No chapters type', 'bkjfielcombr'),
        );
    } 
}

/**
 * Get a list of posts
 *
 * Generic function to return an array of posts formatted for CMB2. Simply pass
 * in your WP_Query arguments and get back a beautifully formatted CMB2 options
 * array.
 *
 * @param array $query_args WP_Query arguments
 * @return array CMB2 options array
 */
function Get_Post_array($query_args = array(), $exclude_current_post = false, $add_none = false) 
{
    $lang = function_exists('pll_current_language') && isset($_GET['post']) ? pll_get_post_language($_GET['post']) : '';
    $exclude_id = $exclude_current_post && isset($_GET['post']) ? array($_GET['post']) : '';

    $defaults = array(
        'posts_per_page' => -1,
        'orderby'          => 'title',
        'order'            => 'ASC',
        'post_status'      => 'publish',
        'post__not_in'     => $exclude_id,
        'lang'             => $lang
    );
    $query = new WP_Query(array_replace_recursive($defaults, $query_args));
    
    if ($add_none) {
        return array("-1" => '') + wp_list_pluck($query->get_posts(), 'post_title', 'ID');
    } else {
        return wp_list_pluck($query->get_posts(), 'post_title', 'ID');
    }
    
}

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

/**********
 * Show on Front Page
 *
 * @return bool $display
 */
function Show_On_Front_page($cmb)
{
    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($front_page) : $front_page;

    // There is a front page set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Home / Blog
 *
 * @return bool $display
 */
function Show_On_Home($cmb)
{
    // Get ID of page set as home, 0 if there isn't one
    $home = get_option('page_for_posts');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($home) : $home;

    // There is a home set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Privacy Policy 
 *
 * @return bool $display
 */
function Show_On_Privacy($cmb)
{
    // Get ID of page set as privacy, 0 if there isn't one
    $privacy = get_option('wp_page_for_privacy_policy');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($privacy) : $privacy;

    // There is a privacy set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/***********************************************************************************
 * Register taxonomies
 * ********************************************************************************/

/***********************************************************************************
 * Register post types
 * ********************************************************************************/

/**********
 * Books
 *********/
function cpt_bkjfiel_book() 
{
    $labels = array(
        'name'                  => __('Books', 'bkjfielcombr'),
        'singular_name'         => __('Book', 'bkjfielcombr'),
        'menu_name'             => __('Books', 'bkjfielcombr'),
        'name_admin_bar'        => __('Book', 'bkjfielcombr'),
        'archives'              => __('Book Archives', 'bkjfielcombr'),
        'attributes'            => __('Book Attributes', 'bkjfielcombr'),
        'parent_item_colon'     => __('Parent Book:', 'bkjfielcombr'),
        'all_items'             => __('All Books', 'bkjfielcombr'),
        'add_new_item'          => __('Add New Book', 'bkjfielcombr'),
        'add_new'               => __('Add New', 'bkjfielcombr'),
        'new_item'              => __('New Book', 'bkjfielcombr'),
        'edit_item'             => __('Edit Book', 'bkjfielcombr'),
        'update_item'           => __('Update Book', 'bkjfielcombr'),
        'view_item'             => __('View Book', 'bkjfielcombr'),
        'view_items'            => __('View Books', 'bkjfielcombr'),
        'search_items'          => __('Search Book', 'bkjfielcombr'),
        'not_found'             => __('Not found', 'bkjfielcombr'),
        'not_found_in_trash'    => __('Not found in Trash', 'bkjfielcombr'),
        'featured_image'        => __('Featured Image', 'bkjfielcombr'),
        'set_featured_image'    => __('Set featured image', 'bkjfielcombr'),
        'remove_featured_image' => __('Remove featured image', 'bkjfielcombr'),
        'use_featured_image'    => __('Use as featured image', 'bkjfielcombr'),
        'insert_into_item'      => __('Insert into book', 'bkjfielcombr'),
        'uploaded_to_this_item' => __('Uploaded to this book', 'bkjfielcombr'),
        'items_list'            => __('Books list', 'bkjfielcombr'),
        'items_list_navigation' => __('Books list navigation', 'bkjfielcombr'),
        'filter_items_list'     => __('Filter books list', 'bkjfielcombr'),
    );
    $rewrite = array(
        'slug'                  => 'livro',
        'with_front'            => false,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Book', 'bkjfielcombr'),
        'description'           => __('Books', 'bkjfielcombr'),
        'labels'                => $labels,
        'supports'              => array('title', 'revisions', 'page-attributes'),
        'taxonomies'            => array(''),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 4,
        'menu_icon'             => 'dashicons-book-alt',
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => false,
        'show_in_rest'          => true,
        'rest_base'             => 'livros',
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type('bkjfiel_book', $args);

}
add_action('init', 'cpt_bkjfiel_book', 0);

/**********
 * Chapters
 *********/
function cpt_bkjfiel_chapter() 
{
    $labels = array(
        'name'                  => __('Chapters', 'bkjfielcombr'),
        'singular_name'         => __('Chapter', 'bkjfielcombr'),
        'menu_name'             => __('Chapters', 'bkjfielcombr'),
        'name_admin_bar'        => __('Chapter', 'bkjfielcombr'),
        'archives'              => __('Chapter Archives', 'bkjfielcombr'),
        'attributes'            => __('Chapter Attributes', 'bkjfielcombr'),
        'parent_item_colon'     => __('Parent Chapter:', 'bkjfielcombr'),
        'all_items'             => __('All Chapters', 'bkjfielcombr'),
        'add_new_item'          => __('Add New Chapter', 'bkjfielcombr'),
        'add_new'               => __('Add New', 'bkjfielcombr'),
        'new_item'              => __('New Chapter', 'bkjfielcombr'),
        'edit_item'             => __('Edit Chapter', 'bkjfielcombr'),
        'update_item'           => __('Update Chapter', 'bkjfielcombr'),
        'view_item'             => __('View Chapter', 'bkjfielcombr'),
        'view_items'            => __('View Chapters', 'bkjfielcombr'),
        'search_items'          => __('Search Chapter', 'bkjfielcombr'),
        'not_found'             => __('Not found', 'bkjfielcombr'),
        'not_found_in_trash'    => __('Not found in Trash', 'bkjfielcombr'),
        'featured_image'        => __('Featured Image', 'bkjfielcombr'),
        'set_featured_image'    => __('Set featured image', 'bkjfielcombr'),
        'remove_featured_image' => __('Remove featured image', 'bkjfielcombr'),
        'use_featured_image'    => __('Use as featured image', 'bkjfielcombr'),
        'insert_into_item'      => __('Insert into chapter', 'bkjfielcombr'),
        'uploaded_to_this_item' => __('Uploaded to this chapter', 'bkjfielcombr'),
        'items_list'            => __('Chapters list', 'bkjfielcombr'),
        'items_list_navigation' => __('Chapters list navigation', 'bkjfielcombr'),
        'filter_items_list'     => __('Filter chapters list', 'bkjfielcombr'),
    );
    $rewrite = array(
        'slug'                  => 'capitulo',
        'with_front'            => false,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Chapter', 'bkjfielcombr'),
        'description'           => __('Chapters', 'bkjfielcombr'),
        'labels'                => $labels,
        'supports'              => array('title', 'revisions', 'page-attributes'),
        'taxonomies'            => array(''),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 4,
        'menu_icon'             => 'dashicons-text-page',
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => false,
        'show_in_rest'          => true,
        'rest_base'             => 'capitulos',
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type('bkjfiel_chapter', $args);

}
add_action('init', 'cpt_bkjfiel_chapter', 0);

/*****
 * Verses
 */
function cpt_bkjfiel_verse() 
{
    $labels = array(
        'name'                  => __('Verses', 'bkjfielcombr'),
        'singular_name'         => __('Verse', 'bkjfielcombr'),
        'menu_name'             => __('Verses', 'bkjfielcombr'),
        'name_admin_bar'        => __('Verse', 'bkjfielcombr'),
        'archives'              => __('Verse Archives', 'bkjfielcombr'),
        'attributes'            => __('Verse Attributes', 'bkjfielcombr'),
        'parent_item_colon'     => __('Parent Verse:', 'bkjfielcombr'),
        'all_items'             => __('All Verses', 'bkjfielcombr'),
        'add_new_item'          => __('Add New Verse', 'bkjfielcombr'),
        'add_new'               => __('Add New', 'bkjfielcombr'),
        'new_item'              => __('New Verse', 'bkjfielcombr'),
        'edit_item'             => __('Edit Verse', 'bkjfielcombr'),
        'update_item'           => __('Update Verse', 'bkjfielcombr'),
        'view_item'             => __('View Verse', 'bkjfielcombr'),
        'view_items'            => __('View Verses', 'bkjfielcombr'),
        'search_items'          => __('Search Verse', 'bkjfielcombr'),
        'not_found'             => __('Not found', 'bkjfielcombr'),
        'not_found_in_trash'    => __('Not found in Trash', 'bkjfielcombr'),
        'featured_image'        => __('Featured Image', 'bkjfielcombr'),
        'set_featured_image'    => __('Set featured image', 'bkjfielcombr'),
        'remove_featured_image' => __('Remove featured image', 'bkjfielcombr'),
        'use_featured_image'    => __('Use as featured image', 'bkjfielcombr'),
        'insert_into_item'      => __('Insert into verse', 'bkjfielcombr'),
        'uploaded_to_this_item' => __('Uploaded to this verse', 'bkjfielcombr'),
        'items_list'            => __('Verses list', 'bkjfielcombr'),
        'items_list_navigation' => __('Verses list navigation', 'bkjfielcombr'),
        'filter_items_list'     => __('Filter verses list', 'bkjfielcombr'),
    );
    $rewrite = array(
        'slug'                  => 'versiculo',
        'with_front'            => false,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Verse', 'bkjfielcombr'),
        'description'           => __('Verses', 'bkjfielcombr'),
        'labels'                => $labels,
        'supports'              => array('title', 'editor', 'revisions', 'page-attributes'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 4,
        'menu_icon'             => 'dashicons-text',
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => false,
        'show_in_rest'          => true,
        'rest_base'             => 'versiculos',
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type('bkjfiel_verse', $args);
}
add_action('init', 'cpt_bkjfiel_verse', 0);

/*****
 * Concordance
 */
function cpt_bkjfiel_concordance() 
{
    $labels = array(
        'name'                  => __('Biblical Concordances', 'bkjfielcombr'),
        'singular_name'         => __('Biblical Concordance', 'bkjfielcombr'),
        'menu_name'             => __('Biblical Concordances', 'bkjfielcombr'),
        'name_admin_bar'        => __('Biblical Concordance', 'bkjfielcombr'),
        'archives'              => __('Concordance Archives', 'bkjfielcombr'),
        'attributes'            => __('Concordance Attributes', 'bkjfielcombr'),
        'parent_item_colon'     => __('Parent Concordance:', 'bkjfielcombr'),
        'all_items'             => __('All Concordances', 'bkjfielcombr'),
        'add_new_item'          => __('Add New Concordance', 'bkjfielcombr'),
        'add_new'               => __('Add New', 'bkjfielcombr'),
        'new_item'              => __('New Concordance', 'bkjfielcombr'),
        'edit_item'             => __('Edit Concordance', 'bkjfielcombr'),
        'update_item'           => __('Update Concordance', 'bkjfielcombr'),
        'view_item'             => __('View Concordance', 'bkjfielcombr'),
        'view_items'            => __('View Concordances', 'bkjfielcombr'),
        'search_items'          => __('Search Concordance', 'bkjfielcombr'),
        'not_found'             => __('Not found', 'bkjfielcombr'),
        'not_found_in_trash'    => __('Not found in Trash', 'bkjfielcombr'),
        'featured_image'        => __('Featured Image', 'bkjfielcombr'),
        'set_featured_image'    => __('Set featured image', 'bkjfielcombr'),
        'remove_featured_image' => __('Remove featured image', 'bkjfielcombr'),
        'use_featured_image'    => __('Use as featured image', 'bkjfielcombr'),
        'insert_into_item'      => __('Insert into concordance', 'bkjfielcombr'),
        'uploaded_to_this_item' => __('Uploaded to this concordance', 'bkjfielcombr'),
        'items_list'            => __('Concordances list', 'bkjfielcombr'),
        'items_list_navigation' => __('Concordances list navigation', 'bkjfielcombr'),
        'filter_items_list'     => __('Filter concordances list', 'bkjfielcombr'),
    );
    $rewrite = array(
        'slug'                  => 'concordancia',
        'with_front'            => false,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Concordance', 'bkjfielcombr'),
        'description'           => __('Concordances', 'bkjfielcombr'),
        'labels'                => $labels,
        'supports'              => array('title', 'editor', 'revisions', 'page-attributes'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-list-view',
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => false,
        'show_in_rest'          => true,
        'rest_base'             => 'concordancias',
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type('bkjfiel_concordance', $args);
}
add_action('init', 'cpt_bkjfiel_concordance', 0);

/***********************************************************************************
 * CPT Fields
 * ********************************************************************************/

/******* 
 * Book
 ********/
function cmb_bkjfiel_book() 
{
    $prefix = '_bkjfiel_book_';
    
    /**
    * Settings
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => '_bkjfiel_book_settings_id',
            'title'         => __('Book Settings', 'bkjfielcombr'),
            'object_types'  => array('bkjfiel_book'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
            //'show_in_rest' => WP_REST_Server::ALLMETHODS,
        )
    );

    //Testament
    $cmb->add_field(
        array(
            'name'    => __('Testament', 'bkjfielcombr'),
            'desc'    => __('Choose Book Testament', 'bkjfielcombr'),
            'id'      => $prefix .'testament',
            'type'    => 'radio',
            'show_option_none' => false,
            'options'          => array(
                'Velho'   => __('Old Testament', 'bkjfielcombr'),
                'Novo'    => __('New Testament', 'bkjfielcombr'),
                'Apocrifo' => __('Apocrypha', 'bkjfielcombr'),
            ),
        ) 
    );
}
add_action('cmb2_admin_init', 'cmb_bkjfiel_book');

/**** 
 * Chapter
 */
function cmb_bkjfiel_chapter() 
{
    $prefix = '_bkjfiel_chapter_';
    
    /**
    * Settings
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => '_bkjfiel_chapter_settings_id',
            'title'         => __('Chapter Settings', 'bkjfielcombr'),
            'object_types'  => array('bkjfiel_chapter'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
            //'show_in_rest' => WP_REST_Server::ALLMETHODS,
        )
    );

    //Book
    $cmb->add_field(
        array(
            'name'           => __('Book', 'bkjfielcombr'),
            'desc'           => __('Select Book of Chapter', 'bkjfielcombr'),
            'id'             => $prefix . 'book',
            'type'           => 'select',
            'options'        => Get_Post_array(array('post_type' => 'bkjfiel_book'), false, true),
        )
    );

    //Chapter Number
    $cmb->add_field(
        array(
            'name'           => __('Chapter Number', 'bkjfielcombr'),
            'desc'           => __('Enter Chapter Number', 'bkjfielcombr'),
            'id'             => $prefix . 'number',
            'type'           => 'text_small',
            'attributes' => array(
                'type' => 'number',
                'min'  => '1',
            ),
        )
    );
}
add_action('cmb2_admin_init', 'cmb_bkjfiel_chapter');


/**** 
 * Verse
 */
function cmb_bkjfiel_verse() 
{
    $prefix = '_bkjfiel_verse_';
    
    /**
    * Settings
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => '_bkjfiel_verse_book_id',
            'title'         => __('Verse Settings', 'bkjfielcombr'),
            'object_types'  => array('bkjfiel_verse'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
            //'show_in_rest' => WP_REST_Server::ALLMETHODS,
        )
    );

    //Chapter
    $cmb->add_field(
        array(
            'name'           => __('Chapter', 'bkjfielcombr'),
            'desc'           => __('Select Chapter of Verse', 'bkjfielcombr'),
            'id'             => $prefix . 'chapter',
            'type'           => 'select',
            'options'        => Get_Post_array(array('post_type' => 'bkjfiel_chapter'), false, true),
        )
    );

    //Verse
    $cmb->add_field(
        array(
            'name'           => __('Verse Number', 'bkjfielcombr'),
            'desc'           => __('Enter Verse Number', 'bkjfielcombr'),
            'id'             => $prefix . 'number',
            'type'           => 'text_small',
            'attributes' => array(
                'type' => 'number',
                'min'  => '1',
            ),
        )
    );
}
add_action('cmb2_admin_init', 'cmb_bkjfiel_verse');

/**** 
 * Concordance
 */
function cmb_bkjfiel_concordance() 
{
    $prefix = '_bkjfiel_concordance_';
    
    /**
    * Biblical Concordances
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => '_bkjfiel_concordance_biblical_id',
            'title'         => __('Concordance Settings', 'bkjfielcombr'),
            'object_types'  => array('bkjfiel_concordance'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
            //'show_in_rest' => WP_REST_Server::ALLMETHODS,
        )
    );

    //Comments
    $cmb->add_field(
        array(
            'name'           => __('Comments', 'bkjfielcombr'),
            'desc'           => __('Word observations (text displayed alongside)', 'bkjfielcombr'),
            'id'             => $prefix . 'comments',
            'type'           => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 2,
            ),
        )
    );

    //Variations
    $cmb->add_field(
        array(
            'name'           => __('Word Variations', 'bkjfielcombr'),
            'desc'           => __('Enter a list of variations of the main word separated by commas (Ex. Amarás, ama, amo, ama, amar, amai)', 'bkjfielcombr'),
            'id'             => $prefix . 'variations',
            'type'           => 'text',
        )
    );

    //Verses Group
    $verse_id = $cmb->add_field(
        array(
            'id'          => $prefix . 'concordances',
            'type'        => 'group',
            'desc' => '',
            'options'     => array(
                'group_title'   =>__('Verse {#}', 'bkjfielcombr'),
                'add_button'   =>__('Add Another Verse', 'bkjfielcombr'),
                'remove_button' =>__('Remove Verse', 'bkjfielcombr'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Verse
    $cmb->add_group_field(
        $verse_id,
        array(
            'name'           => __('Verse', 'bkjfielcombr'),
            'desc'           => __('Select a Verse', 'bkjfielcombr'),
            'id'             => 'verse',
            'type'           => 'select',
            'options'        => Get_Post_array(array('post_type' => 'bkjfiel_verse'), false, true),
        )
    );

    //Content
    $cmb->add_group_field(
        $verse_id,
        array(
            'name'           => __('Content', 'bkjfielcombr'),
            'desc'           => __('Or insert content', 'bkjfielcombr'),
            'id'             => 'content',
            'type'           => 'textarea_code',
            'options' => array(
                'textarea_rows' => 5,
            ),
        )
    );

}
add_action('cmb2_admin_init', 'cmb_bkjfiel_concordance');

/***********************************************************************************
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_bkjfiel_frontpage_';
        $show_on = 'Show_On_Front_page';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Banners', 'bkjfielcombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'heros',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   => __('Slide {#}', 'bkjfielcombr'),
                    'add_button'   => __('Add Another Slide', 'bkjfielcombr'),
                    'remove_button' => __('Remove Slide', 'bkjfielcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'bkjfielcombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb'    => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image (Mobile)', 'bkjfielcombr'),
                'desc'        => __('Format: JPG (recommended) or PNG | Recommended size: 1080x1350px | Aspect ratio: 4:5', 'bkjfielcombr'),
                'id'          => 'image_mobile',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Upload Image', 'bkjfielcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 350)
            )
        );

        //Hero Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image (Desktop)', 'bkjfielcombr'),
                'desc'        => __('Format: JPG (recommended) or PNG | Recommended size: 2560x540px | Aspect ratio: 128:27', 'bkjfielcombr'),
                'id'          => 'image_desktop',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Upload Image', 'bkjfielcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 350)
            )
        );

        //Hero Link
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Link (URL)', 'bkjfielcombr'),
                'desc'       => '',
                'id'         => 'target_link_url',
                'type'       => 'text',
            )
        );
    }
);

/**** 
 * Booklet
 */
function cmb_bkjfiel_booklet() 
{
    $prefix = '_bkjfiel_booklet_';

    /******
     * Content
     ******/
    $cmb_content = new_cmb2_box(
        array(
            'id'            => '_bkjfiel_booklet_content_id',
            'title'         => __('Content', 'bkjfielcombr'),
            'object_types'  => array('page'), // post type
            'show_on'       => array('key' => 'page-template', 'value' => 'page-livreto.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Content Title
    $cmb_content->add_field(
        array(
            'name'       => __('Title', 'bkjfielcombr'),
            'desc'       => '',
            'id'         => $prefix . 'content_title',
            'type'       => 'textarea_code',
            'options' => array(
                'textarea_rows' => 2,
            ),
        )
    );

    //Content Subtitle
    $cmb_content->add_field(
        array(
            'name'       => __('Subtitle', 'bkjfielcombr'),
            'desc'       => '',
            'id'         => $prefix . 'content_subtitle',
            'type'       => 'textarea_code',
            'options' => array(
                'textarea_rows' => 2,
            ),
        )
    );

    //Content
    /*$cmb_content->add_field(
        array(
            'name'       => __('Content', 'bkjfielcombr'),
            'desc'       => '',
            'id'         => $prefix . 'content',
            'type'       => 'wysiwyg',
            'options' => array(
                'media_buttons' => true,
                'textarea_rows' => get_option('default_post_edit_rows', 25),
                'tabindex' => '',
                'editor_css' => '',
                'editor_class' => '',
                'teeny' => true,
                'tinymce' => true,
                'quicktags' => true
            ),
        )
    );
    */

    //Booklet PDF
    $cmb_content->add_field(
        array(
            'name'    => __('Booklet PDF', 'bkjfielcombr'),
            'desc'    => __('Upload PDF file', 'bkjfielcombr'),
            'id'      => $prefix . 'pdf_file',
            'type'    => 'file',
            'options' => array(
                'url' => false,
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add PDF File', 'bkjfielcombr'),
            ),
            'query_args' => array(
                'type' => 'application/pdf'
            ),
            'preview_size' => 'medium',
        )
    );

    //Booklet EPUB
    $cmb_content->add_field(
        array(
            'name'    => __('Booklet EPUB', 'bkjfielcombr'),
            'desc'    => __('Upload EPUB file', 'bkjfielcombr'),
            'id'      => $prefix . 'epub_file',
            'type'    => 'file',
            'options' => array(
                'url' => false,
            ),
            'text'    => array(
                'add_upload_file_text' => __('Add EPUB File', 'bkjfielcombr'),
            ),
            'query_args' => array(
                'type' => 'application/epub+zip',
            ),
            'preview_size' => 'medium',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_bkjfiel_booklet');

/**** 
 * Popular Verses
 */
function cmb_bkjfiel_popular_verses() 
{
    $prefix = '_bkjfiel_popular_verses_';

    /******
     * Content
     ******/
    $cmb_content = new_cmb2_box(
        array(
            'id'            => $prefix . 'content_id',
            'title'         => __('Content', 'bkjfielcombr'),
            'object_types'  => array('page'), // post type
            'show_on'       => array('key' => 'page-template', 'value' => 'page-versiculos-populares.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Content Title
    $cmb_content->add_field(
        array(
            'name'       => __('Title', 'bkjfielcombr'),
            'desc'       => '',
            'id'         => $prefix . 'content_title',
            'type'       => 'textarea_code',
            'options' => array(
                'textarea_rows' => 2,
            ),
        )
    );

    //Content Subtitle
    $cmb_content->add_field(
        array(
            'name'       => __('Subtitle', 'bkjfielcombr'),
            'desc'       => '',
            'id'         => $prefix . 'content_subtitle',
            'type'       => 'textarea_code',
            'options' => array(
                'textarea_rows' => 2,
            ),
        )
    );

    /******
    * Popular Verses
    ******/
    $cmb_verses = new_cmb2_box(
        array(
            'id'            => $prefix . '_verses_id',
            'title'         => __('Popular Verses', 'bkjfielcombr'),
            'object_types'  => array('page'), // post type
            'show_on'       => array('key' => 'page-template', 'value' => 'page-versiculos-populares.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Verses Group
    $verse_id = $cmb_verses->add_field(
        array(
            'id'          => $prefix . 'verses',
            'type'        => 'group',
            'desc' => '',
            'options'     => array(
                'group_title'   =>__('Verse {#}', 'bkjfielcombr'),
                'add_button'   =>__('Add Another Verse', 'bkjfielcombr'),
                'remove_button' =>__('Remove Verse', 'bkjfielcombr'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Chapter
    $cmb_verses->add_group_field(
        $verse_id,
        array(
            'name'           => __('Chapter', 'bkjfielcombr'),
            'desc'           => __('Select Chapter of Verse', 'bkjfielcombr'),
            'id'             => 'chapter',
            'type'           => 'select',
            'options'        => Get_Post_array(array('post_type' => 'bkjfiel_chapter'), false, true),
        )
    );

    //Verse
    $cmb_verses->add_group_field(
        $verse_id,
        array(
            'name'           => __('Verse Number', 'bkjfielcombr'),
            'desc'           => __('Enter Verse Number', 'bkjfielcombr'),
            'id'             => 'verse',
            'type'           => 'text_small',
            'attributes' => array(
                'type' => 'number',
                'min'  => '1',
            ),
        )
    );
}
add_action('cmb2_admin_init', 'cmb_bkjfiel_popular_verses');


/**** 
 * Concordance
 */
function cmb_bkjfiel_concordance_biblical() 
{
    $prefix = '_bkjfiel_concordance_';

    /******
     * Content
     ******/
    $cmb_content = new_cmb2_box(
        array(
            'id'            => $prefix . 'content_id',
            'title'         => __('Content', 'bkjfielcombr'),
            'object_types'  => array('page'), // post type
            'show_on'       => array('key' => 'page-template', 'value' => 'page-concordancia.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Content Title
    $cmb_content->add_field(
        array(
            'name'       => __('Title', 'bkjfielcombr'),
            'desc'       => '',
            'id'         => $prefix . 'content_title',
            'type'       => 'textarea_code',
            'options' => array(
                'textarea_rows' => 2,
            ),
        )
    );

    //Content Subtitle
    $cmb_content->add_field(
        array(
            'name'       => __('Subtitle', 'bkjfielcombr'),
            'desc'       => '',
            'id'         => $prefix . 'content_subtitle',
            'type'       => 'textarea_code',
            'options' => array(
                'textarea_rows' => 2,
            ),
        )
    );
}
add_action('cmb2_admin_init', 'cmb_bkjfiel_concordance_biblical');

/**** 
 * Verse of Day
 */
function cmb_bkjfiel_verses_of_day() 
{
    $prefix = '_bkjfiel_verse_of_day_';

    /******
    * Verses of Day
    ******/
    $cmb_verses = new_cmb2_box(
        array(
            'id'            => $prefix . '_verses_id',
            'title'         => __('Verses of Day', 'bkjfielcombr'),
            'object_types'  => array('page'), // post type
            'show_on'       => array('key' => 'page-template', 'value' => 'page-versiculo-do-dia.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            //'show_in_rest' => WP_REST_Server::ALLMETHODS,
        )
    );

    //Verses Group
    $verse_id = $cmb_verses->add_field(
        array(
            'id'          => $prefix . 'verses',
            'type'        => 'group',
            'desc' => '',
            'options'     => array(
                'group_title'   =>__('Day {#}', 'bkjfielcombr'),
                'add_button'   =>__('Add Another Verse', 'bkjfielcombr'),
                'remove_button' =>__('Remove Verse', 'bkjfielcombr'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Chapter
    $cmb_verses->add_group_field(
        $verse_id,
        array(
            'name'           => __('Chapter', 'bkjfielcombr'),
            'desc'           => __('Select Chapter of Verse', 'bkjfielcombr'),
            'id'             => 'chapter',
            'type'           => 'select',
            'options'        => Get_Post_array(array('post_type' => 'bkjfiel_chapter'), false, true),
        )
    );

    //Verse
    $cmb_verses->add_group_field(
        $verse_id,
        array(
            'name'           => __('Verse Number', 'bkjfielcombr'),
            'desc'           => __('Enter Verse Number', 'bkjfielcombr'),
            'id'             => 'verse',
            'type'           => 'text_small',
            'attributes' => array(
                'type' => 'number',
                'min'  => '1',
            ),
        )
    );

    //Final Verse Number
    $cmb_verses->add_group_field(
        $verse_id,
        array(
            'name'           => __('Final Verse Number', 'bkjfielcombr'),
            'desc'           => __('Enter Final Verse Number', 'bkjfielcombr'),
            'id'             => 'verse_final',
            'type'           => 'text_small',
            'attributes' => array(
                'type' => 'number',
                'min'  => '0',
            ),
        )
    );
}
add_action('cmb2_admin_init', 'cmb_bkjfiel_verses_of_day');

/**
 * Prayer Request
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_bkjfiel_prayerrequests_';
        $show_on = array('key' => 'page-template', 'value' => 'page-pedidos-de-oracao.blade.php');

        /****** 
        * Contacts
        ******/
        $cmb_prayerrequests = new_cmb2_box(
            array(
                'id'            => $prefix . 'prayerrequests_id',
                'title'         => __('Contact', 'bkjfielcombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Shortcode
        $cmb_prayerrequests->add_field( 
            array(
                'name'       => __('Form Shortcode', 'bkjfielcombr'),
                'desc'       => __('Contact Form 7 Shortcode', 'bkjfielcombr'),
                'id'         => $prefix . 'form_shortcode',
                'type'       => 'text',
            )
        );
    }
);

/**
 * Contact
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_bkjfiel_contact_';
        $show_on = array('key' => 'page-template', 'value' => 'page-contato.blade.php');

        /****** 
        * Contacts
        ******/
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => $prefix . 'contact_id',
                'title'         => __('Contact', 'bkjfielcombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Shortcode
        $cmb_contact->add_field( 
            array(
                'name'       => __('Form Shortcode', 'bkjfielcombr'),
                'desc'       => __('Contact Form 7 Shortcode', 'bkjfielcombr'),
                'id'         => $prefix . 'form_shortcode',
                'type'       => 'text',
            )
        );
    }
);
